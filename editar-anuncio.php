<?php require('partials/header.php');
require('classes/Anuncios.php');

if (empty($_SESSION['login'])) {
    ?>
        <script type="text/javascript">window.locategoriaion.href= './';</script>
    <?php 
}

$a = new Anuncios();

if (!empty($_POST['categoria']) && !empty($_POST['titulo']) && !empty($_POST['valor']) && !empty($_POST['estado']) && !empty($_POST['descricao'])) {
    $titulo = addslashes($_POST['titulo']);
    $valor = addslashes($_POST['valor']);
    $descricao = addslashes($_POST['descricao']);
    $estado = addslashes($_POST['estado']);
    $categoria = addslashes($_POST['categoria']);
    if ($a->editarAnuncio($_GET['id'], $titulo, $valor, $descricao, $estado, $categoria)) {
        ?>
            <div class="alert alert-success" role="alert">
                Anúncio editado com sucesso
            </div>
        <?php

    }
}

if (!empty($_GET['id']))
    $anuncioParaEditar = $a->buscarAnuncio(addslashes($_GET['id']));
else {
    ?>
        <script type="text/javascript">window.locategoriaion.href= './';</script>
    <?php 
}
?>

<div class="container mt-4">
    <form method="POST" enctype="multipart/form-data">
        <div class="row my-3">
            <div class="col-md">
                <h3 class="text-center pb-3">Editar Anúncio</h3>
            </div>
        </div>
        <div class="row my-3">
            <div class="col-md">
                <div class="form-group">
                    <label for="categoria">Categoria</label>
                    <select name="categoria" id="categoria" class="form-control">
                        <?php 
                        require('classes/Categorias.php');
                        $cats = new Categorias();
                        foreach ($cats->listarCategorias() as $cat) {
                            ?>
                            <option value="<?php echo ($cat['id']); ?>"
                                <?php echo ($anuncioParaEditar['id_categoria'] === $cat['id'] ? 'selected' : '') ?>
                            >
                                <?php echo ($cat['nome']); ?>
                            </option>
                            <?php

                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="col-md">
                <label for="titulo">Título</label>
                <input type="text" name="titulo" id="titulo" class="form-control" value="<?php echo ($anuncioParaEditar['titulo']); ?>"/>
            </div>
        </div>
        <div class="row my-3">
            <div class="col-md">
                <label for="valor">Valor</label>
                <input type="number" name="valor" id="valor" class="form-control" min="0" value="<?php echo ($anuncioParaEditar['preco']); ?>"/>
            </div>
            <div class="col-md">
                <label for="estado">Estado de Conservação</label>
                <select name="estado" id="estado" class="form-control">
                    <option value="0"
                        <?php echo ($anuncioParaEditar['estado'] == 0 ? 'selected' : ''); ?>
                    >Ruim</option>
                    <option value="1"
                        <?php echo ($anuncioParaEditar['estado'] == 1 ? 'selected' : ''); ?>
                    >Bom</option>
                    <option value="2"
                        <?php echo ($anuncioParaEditar['estado'] == 2 ? 'selected' : ''); ?>
                    >Ótimo</option>
                </select>
            </div>
        </div>
        <div class="row my-3">
            <div class="col-md">
                <label for="descricao">Descrição</label>
                <textarea name="descricao" id="descricao" class="form-control" rows="7" draggable="false">
                    <?php echo ($anuncioParaEditar['descricao']); ?> 
                </textarea>
            </div>
        </div>
        <input type="submit" value="Salvar" class="btn btn-success btn-block" />
    </form>
</div>
<?php require('partials/footer.php'); ?>