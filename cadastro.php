<?php require('partials/header.php'); ?>
<?php 
if (isset($_SESSION['login']) && !empty(isset($_SESSION['login']))) {
    ?>
        <script type="text/javascript">window.location.href= './';</script>
    <?php

}
?>
<div class="container mt-5">
    <h3 class="text-center text-md-left">Cadastre-se</h3>
    <?php 
    require('classes/Usuarios.php');
    $user = new Usuarios();
    if (isset($_POST['nome']) && !empty($_POST['nome'])) {
        $nome = addslashes($_POST['nome']);
        $email = addslashes($_POST['email']);
        $senha = $_POST['senha'];
        $telefone = addslashes($_POST['telefone']);
        if (!empty($nome) && !empty($email) && !empty($senha)) {
            if ($user->cadastrar($nome, $email, $senha, $telefone)) {
                ?>
                    <div class="alert alert-success" role="alert">
                        Usuário Cadastrado <br>
                        <a href="login.php" class="alert-link">Fazer login</a>
                    </div>
                <?php

            } else {
                ?>
                    <div class="alert alert-danger" role="alert">
                        Email já cadastrado<br>
                        <a href="login.php" class="alert-link">Fazer login</a>
                    </div>
                <?php

            }
        } else {
            ?>
                <div class="alert alert-warning" role="alert">
                    Preencha todos os campos
                </div>
            <?php

        }
    }
    ?>
    <form method="POST">
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                <label for="nome">Nome:</label>
                <input type="text" class="form-control" id="nome" name="nome"/>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="email">Email:</label>
                    <input type="email" class="form-control" id="email" name="email"/>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                <label for="senha">Senha:</label>
                <input type="password" class="form-control" id="senha" name="senha"/>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="telefone">Telefone:</label>
                    <input type="text" class="form-control" id="telefone" name="telefone"/>
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-success btn-block">Cadastrar</button>
    </form>
</div>

<?php require('partials/footer.php'); ?>