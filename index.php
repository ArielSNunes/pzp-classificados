<?php require('partials/header.php'); ?>
<section class="jumbotron jumbotron-fluid">
    <div class="container">
        <h1 class="display-4">Nós temos mais de 999 anúncios</h1>
        <p class="lead">E mais de 999 usuários cadastrados</p>
    </div>
</section>
<!-- section.jumbotron -->
<section class="search">
    <section class="container">
        <div class="row">
            <div class="col-sm-3">
                <h4>Pesquisa avançada</h4>
            </div>
            <div class="col-sm-9">
                <h4>Últimos anúncios</h4>
            </div>
        </div>
    </section>
</section>
<!-- section.search -->
<?php require('partials/footer.php'); ?>