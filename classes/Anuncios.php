<?php 
class Anuncios
{
    public function listarAnunciosDoUsuario()
    {
        global $pdo;
        $arr = [];
        $sql = "SELECT 
            `anuncios`.`id` AS id_anuncio,
            `anuncios`.`titulo` AS titulo_anuncio, 
            `anuncios`.`preco` AS preco_anuncio,
            `anuncios_imagens`.`url` AS foto_anuncio,
            `categorias`.`nome` AS categoria
        FROM `anuncios`
        LEFT JOIN `anuncios_imagens` ON `anuncios`.`id` = `anuncios_imagens`.`id_anuncio`
        LEFT JOIN `categorias` ON `categorias`.`id` = `anuncios`.`id_categoria`
        WHERE `anuncios`.`id_usuario` = :idUsuario";
        $sql = $pdo->prepare($sql);
        $sql->bindValue('idUsuario', $_SESSION['login']['id']);
        $sql->execute();
        if ($sql->rowCount() > 0)
            $arr = $sql->fetchAll();
        return $arr;
    }
    public function criarAnuncio($titulo, $categoria, $valor, $estado, $descricao)
    {
        global $pdo;
        $sql = "INSERT INTO anuncios (id_usuario, id_categoria, titulo, descricao, preco, estado) VALUES (:idUsuario, :idCat, :titulo, :descricao, :preco, :estado)";
        $sql = $pdo->prepare($sql);
        $sql->bindValue('idUsuario', $_SESSION['login']['id']);
        $sql->bindValue('idCat', $categoria);
        $sql->bindValue('titulo', $titulo);
        $sql->bindValue('descricao', $descricao);
        $sql->bindValue('preco', number_format($valor, 2));
        $sql->bindValue('estado', $estado);
        $sql->execute();
        return true;
    }
    public function removerAnuncio($id)
    {
        global $pdo;
        $sql = "DELETE from anuncios_imagens WHERE id_anuncio = :id";
        $sql = $pdo->prepare($sql);
        $sql->bindValue('id', $id);
        $sql->execute();

        $sql = "DELETE from anuncios WHERE id = :id";
        $sql = $pdo->prepare($sql);
        $sql->bindValue('id', $id);
        $sql->execute();
        return true;
    }
    public function buscarAnuncio($id)
    {
        global $pdo;
        $sql = $pdo->prepare('SELECT * FROM anuncios WHERE id = :id AND id_usuario = :idUsuario');
        $sql->bindValue('id', $id);
        $sql->bindValue('idUsuario', $_SESSION['login']['id']);
        $sql->execute();
        if ($sql->rowCount() > 0)
            return $sql->fetch();
        return false;
    }
    public function editarAnuncio($id, $titulo, $valor, $descricao, $estado, $categoria)
    {
        global $pdo;
        $sql = "UPDATE anuncios SET id_categoria = :idCat, titulo = :titulo, descricao = :descricao, preco = :preco, estado = :estado WHERE id = :id";
        $sql = $pdo->prepare($sql);
        $sql->bindValue('idCat', $categoria);
        $sql->bindValue('titulo', $titulo);
        $sql->bindValue('descricao', $descricao);
        $sql->bindValue('preco', number_format($valor, 2));
        $sql->bindValue('estado', $estado);
        $sql->bindValue('id', $id);
        $sql->execute();
        return true;

    }
}