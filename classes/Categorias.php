<?php

class Categorias
{
    public function listarCategorias()
    {
        global $pdo;
        $arr = [];
        $sql = $pdo->query('SELECT * FROM categorias ORDER BY id asc');
        if ($sql->rowCount() > 0)
            $arr = $sql->fetchAll();
        return $arr;
    }
}