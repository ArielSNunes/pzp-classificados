<?php

class Usuarios
{

    public function cadastrar($nome, $email, $senha, $telefone = '')
    {
        global $pdo;
        if ($this->verificaSeEmailExiste($email)) {
            return false;
        }
        $sql = "INSERT INTO usuarios (nome, email, senha, telefone) VALUES (:nome, :email, :senha, :telefone)";
        $sql = $pdo->prepare($sql);
        $sql->bindValue('nome', $nome);
        $sql->bindValue('email', $email);
        $sql->bindValue('senha', md5($senha));
        $sql->bindValue('telefone', $telefone);
        $sql->execute();
        return true;
    }
    private function verificaSeEmailExiste($email)
    {
        global $pdo;
        $sql = "SELECT id FROM usuarios WHERE email = :email";
        $sql = $pdo->prepare($sql);
        $sql->bindValue('email', $email);
        $sql->execute();
        if ($sql->rowCount() > 0)
            return true;
        return false;
    }
    public function fazerLogin($email, $senha)
    {
        global $pdo;
        if (!$this->verificaSeEmailExiste($email))
            return false;
        $sql = "SELECT id, nome FROM usuarios WHERE email = :email AND senha = :senha";
        $sql = $pdo->prepare($sql);
        $sql->bindValue('email', $email);
        $sql->bindValue('senha', md5($senha));
        $sql->execute();
        if ($sql->rowCount() <= 0)
            return false;
        $usuario = $sql->fetch();
        $_SESSION['login'] = ['id' => $usuario['id'], 'nome' => $usuario['nome']];
        return true;
    }
}