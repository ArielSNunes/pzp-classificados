drop database proj_classificados;
create database proj_classificados;
use proj_classificados;

create table usuarios (
    id int not null auto_increment,
    nome varchar(100) not null,
    email varchar(100) not null unique,
    senha varchar(32) not null,
    telefone varchar(15),
    primary key (id)
) engine=InnoDB;

create table categorias (
    id int not null auto_increment,
    nome varchar(100) not null unique,
    primary key (id)
) engine=InnoDB;

create table anuncios (
    id int not null auto_increment,
    id_usuario int,
    id_categoria int,
    titulo varchar(100) not null,
    descricao text not null,
    preco float not null,
    estado int not null,
    primary key (id)
) engine=InnoDB;

alter table anuncios add constraint fk_id_usuario foreign key (id_usuario) references usuarios(id);
alter table anuncios add constraint fk_id_categoria foreign key (id_categoria) references categorias(id);

create table anuncios_imagens (
    id int not null auto_increment,
    id_anuncio int,
    url varchar(100) not null,
    primary key (id)
);

alter table anuncios_imagens add constraint fk_id_uanuncio foreign key (id_anuncio) references anuncios(id);

-- Join para tabela dos meus anuncios

SELECT 
    `anuncios`.`id` AS id_anuncio,
    `anuncios`.`titulo` AS titulo_anuncio, 
    `anuncios`.`preco` AS preco_anuncio, 
    `anuncios_imagens`.`url` AS foto_anuncio,
    `categorias`.`nome` as categoria
FROM `anuncios`
LEFT JOIN `anuncios_imagens` ON `anuncios`.`id` = `anuncios_imagens`.`id_anuncio`
LEFT JOIN `categorias` ON `categorias`.`id` = `anuncios`.`id_categoria`
WHERE `anuncios`.`id_usuario` = 1;