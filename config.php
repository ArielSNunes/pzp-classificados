<?php
session_start();

try {
    global $pdo;
    $pdo = new PDO('mysql:host=127.0.0.1;port=3306;dbname=proj_classificados', 'ArielSNunes', 'Ariel', [PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC]);
} catch (PDOException $err) {
    die("Erro: " . $err->getMessage());
}