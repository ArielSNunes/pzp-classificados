<?php require('partials/header.php'); ?>
<?php 
if (empty(isset($_SESSION['login']))) {
    ?>
        <script type="text/javascript">window.locategoriaion.href= './';</script>
    <?php 
}
require('classes/Anuncios.php');
$a = new Anuncios();
if (!empty($_POST['categoria']) && !empty($_POST['titulo']) && !empty($_POST['valor']) && !empty($_POST['estado']) && !empty($_POST['descricao'])) {
    $titulo = addslashes($_POST['titulo']);
    $valor = addslashes($_POST['valor']);
    $descricao = addslashes($_POST['descricao']);
    $estado = addslashes($_POST['estado']);
    $categoria = addslashes($_POST['categoria']);

    if ($a->criarAnuncio($titulo, $categoria, $valor, $estado, $descricao)) {
        ?>
        <div class="alert alert-success" role="alert">
            Anúncio criado com sucesso
        </div>
        <?php

    }
}
?>

<div class="container mt-4">
    <form method="POST" enctype="multipart/form-data">
        <div class="row my-3">
            <div class="col-md">
                <h3 class="text-center pb-3">Adicionar Anúncio</h3>
            </div>
        </div>
        <div class="row my-3">
            <div class="col-md">
                <div class="form-group">
                    <label for="categoria">Categoria</label>
                    <select name="categoria" id="categoria" class="form-control">
                        <?php 
                        require('classes/Categorias.php');
                        $cats = new Categorias();
                        foreach ($cats->listarCategorias() as $cat) {
                            ?>
                            <option value="<?php echo ($cat['id']); ?>">
                                <?php echo ($cat['nome']); ?>
                            </option>
                            <?php

                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="col-md">
                <label for="titulo">Título</label>
                <input type="text" name="titulo" id="titulo" class="form-control" />
            </div>
        </div>
        <div class="row my-3">
            <div class="col-md">
                <label for="valor">Valor</label>
                <input type="number" name="valor" id="valor" class="form-control" min="0"/>
            </div>
            <div class="col-md">
                <label for="estado">Estado de Conservação</label>
                <select name="estado" id="estado" class="form-control">
                    <option value="0">Ruim</option>
                    <option value="1">Bom</option>
                    <option value="2">Ótimo</option>
                </select>
            </div>
        </div>
        <div class="row my-3">
            <div class="col-md">
                <label for="descricao">Descrição</label>
                <textarea name="descricao" id="descricao" class="form-control" rows="7" draggable="false"></textarea>
            </div>
        </div>
        <input type="submit" value="Adicionar" class="btn btn-success btn-block" />
    </form>
</div>
<?php require('partials/footer.php'); ?>