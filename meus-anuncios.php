<?php require('partials/header.php'); ?>
<?php 
if (empty(isset($_SESSION['login']))) {
    ?>
        <script type="text/javascript">window.location.href= './';</script>
    <?php

}
?>
<div class="container mt-4">
    <h3 class="text-center pb-3">Meus Anúncios</h3>
    <a href="novoAnuncio.php" class="btn btn-success btn-block mb-3">Novo Anúncio</a>
    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Categoria</th>
                <th scope="col">Foto</th>
                <th scope="col">Título</th>
                <th scope="col">Valor</th>
                <th scope="col">Ações</th>
            </tr>
        </thead>
        <tbody>
        <?php
        require('classes/Anuncios.php');
        $anuncios = new Anuncios();
        foreach ($anuncios->listarAnunciosDoUsuario() as $anuncio) {
            ?>
                <tr>
                    <td>
                        <?php echo ($anuncio['id_anuncio']) ?>
                    </td>
                    <td>
                        <?php echo ($anuncio['categoria']) ?>
                    </td>
                    <td>
                        <?php if (!empty($anuncio['foto_anuncio'])) { ?>
                                <img src="assets/images/anuncios<?php echo ($anuncio['foto_anuncio']) ?>" 
                                    alt="<?php echo ($anuncio['titulo_anuncio']) ?>" border="0" />
                                <?php 
                            } else { ?>
                                <img src="assets/images/default.jpg" height="50px"
                                    alt="<?php echo ($anuncio['titulo_anuncio']) ?>" border="0" />  
                                <?php 
                            } ?>
                    </td>
                    <td>
                        <?php echo ($anuncio['titulo_anuncio']) ?>
                    </td>
                    <td>
                        R$ <?php echo (number_format($anuncio['preco_anuncio'], 2)); ?>
                    </td>
                    <td>
                        <a class="btn btn-primary" href="editar-anuncio.php?id=<?php echo($anuncio['id_anuncio'])?>">Editar</a>
                        <a class="btn btn-danger"  href="excluir-anuncio.php?id=<?php echo($anuncio['id_anuncio'])?>">Excluir</a>
                    </td>
                </tr>
            <?php

        }
        ?>
        </tbody>
    </table>
</div>

<?php require('partials/footer.php'); ?>