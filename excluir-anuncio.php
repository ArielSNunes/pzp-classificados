<?php 
require('config.php');
require('classes/Anuncios.php');

if (empty($_SESSION['login']))
    header("Location: login.php");

$a = new Anuncios();
if (!empty($_GET['id']))
    $a->removerAnuncio(addslashes($_GET['id']));
header("Location: meus-anuncios.php");