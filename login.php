<?php require('partials/header.php'); ?>
<?php 
if (isset($_SESSION['login']) && !empty(isset($_SESSION['login']))) {
    ?>
        <script type="text/javascript">window.location.href= './';</script>
    <?php

}
?>
<div class="container mt-5">
    <h3 class="text-center text-md-left">Login</h3>
    <?php 
    require('classes/Usuarios.php');
    $user = new Usuarios();
    if (isset($_POST['email']) && !empty($_POST['senha'])) {
        $email = addslashes($_POST['email']);
        $senha = $_POST['senha'];
        if ($user->fazerLogin($email, $senha)) {
            ?>
            <script type="text/javascript">window.location.href= './';</script>
            <?php

        } else {
            ?>
            <div class="alert alert-danger" role="alert">
                Email e/ou senha incorretos<br>
                <a href="login.php" class="alert-link">Fazer login</a>
            </div>
            <?php

        }
    }
    ?>
    <form method="POST">
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="email">Email:</label>
                    <input type="email" class="form-control" id="email" name="email"/>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                <label for="senha">Senha:</label>
                <input type="password" class="form-control" id="senha" name="senha"/>
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-success btn-block">Fazer Login</button>
    </form>
</div>

<?php require('partials/footer.php'); ?>